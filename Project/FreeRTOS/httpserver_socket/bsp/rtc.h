/******************************************************************************

                  版权所有 (C) 2018, 金通科技

 ******************************************************************************
  文件   : rtc.h
  版本   : 初稿
  作者   : zhoujie
  日期   : 2018年1月21日
  修改   :
  描述   : rtc.c 头文件

  修改记录:
  日期   : 2018年1月21日
    作者   : zhoujie
    内容   : 创建文件

******************************************************************************/


#ifndef __RTC_H__
#define __RTC_H__


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern void RTC_AlarmShow(void);
extern void rtc_init1();
extern void RTC_TimeShow(void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __RTC_H__ */
