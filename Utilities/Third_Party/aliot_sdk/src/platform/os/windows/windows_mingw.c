#include <process.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <memory.h>
#include "aliot_platform_os.h"

/* inline */ __int64 GetTickCount64()
	{
		LARGE_INTEGER TicksPerSecond = { 0 };
		LARGE_INTEGER Tick;
		if (!TicksPerSecond.QuadPart)
			QueryPerformanceFrequency(&TicksPerSecond);
		QueryPerformanceCounter(&Tick);
		__int64 Seconds = Tick.QuadPart / TicksPerSecond.QuadPart;
		__int64 LeftPart = Tick.QuadPart - (TicksPerSecond.QuadPart*Seconds);
		__int64 MillSeconds = LeftPart * 1000 / TicksPerSecond.QuadPart;
		__int64 Ret = Seconds * 1000 + MillSeconds;
		return Ret;
	};