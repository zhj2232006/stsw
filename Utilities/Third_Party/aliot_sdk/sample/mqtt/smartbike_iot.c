#include <stdint.h>
#include <stdlib.h>
#include "aliot_log.h"
#include "smartbike_iot.h"
#if defined(_WIN32)
#include <windows.h>
#else
#include "freertos.h"
#endif
#define BE_TO_LE16(X) ((uint16_t)((((uint16_t)(X) & (uint16_t)0x00ffU) << 8) | (((uint16_t)(X) & (uint16_t)0xff00U) >> 8)))
#define BE_TO_LE32(X) ((((uint32_t)(X)&0xff000000) >> 24) | (((uint32_t)(X)&0x00ff0000) >> 8) | (((uint32_t)(X)&0x0000ff00) << 8) | (((uint32_t)(X)&0x000000ff) << 24))
uint32_t g_battery_volt;        // 电池电压
uint32_t g_longitude ;       // 经度
uint32_t g_latitude  ;       // 纬度
uint8_t g_ble_rssi;             // ble rssi
uint16_t g_lock_times;          // 开关锁次数
 const uint32_t g_deviceid;   // 设备id 同 设备名称
// 生成指定区间的随机数
uint32_t gen_rand_range(uint32_t base_min, uint32_t base_max)
{
	return 0;
}
/*****************************************************************************
 函数  : gen_rand_n
 描述  : 生成随机数 注意，分配的 output_rand 内存至少为 rand_type_len*rand_count 字节
 输入  : uint8_t rand_count      需要生成的随机数个数
       uint8_t rand_type_len   随机数的长度
       uint8_t* output_rand    输出参数，存放生成好的随机数
 输出  : 无
 返回  :




*****************************************************************************/
void gen_rand_n(uint8_t rand_count, uint8_t rand_type_len, uint8_t* output_rand)
{
    int i;
    int j;
    uint8_t temp;
    for (j = 0; j < rand_count; j++)
    {
        /* 输出n个随机数，每个长度为 rand_type_len 字节*/
        for (i = 0; i < rand_type_len; i++)
        {
            aliot_platform_msleep(6);

            temp = rand() % 254 + 1;
            *(output_rand++) = temp;
            //ALIOT_LOG_DEBUG("rand=%#x", temp);
        }
    }
    return;
}
void simu_battery_level()
{

    return;
}
void simu_gnss_data()
{
    g_longitude = gen_rand_range(120197983, 120197999);
    g_latitude = gen_rand_range(30169886, 30169999);
    ALIOT_LOG_INFO("longitude = %d", g_longitude);
    ALIOT_LOG_INFO("latitude = %d", g_latitude);
    return;
}
void simu_ble_rssi()
{
    g_ble_rssi = gen_rand_range(1, 100);
    ALIOT_LOG_INFO("rssi = %d", g_ble_rssi);
    return;
}
void simu_lock_times()
{
    g_lock_times = gen_rand_range(1, UINT16_MAX);
    ALIOT_LOG_INFO("lock_times = %d", g_lock_times);
    return;
}
uint32_t simu_bike_mac_addr(BLE_MAC_Addr_t* BLE_MAC)
{
    uint32_t bike_cnt;
    // 周边车辆的蓝牙MAC地址
    bike_cnt = gen_rand_range(1, BLE_MAC_ADDR_NUM_MAX);
    uint8_t mac[6];
#if defined(_WIN32)
    gen_rand_n(6, 1, mac);
#endif
    memcpy(BLE_MAC->MAC, mac, 6);
    aliot_platform_printhex("MAC", mac, 6);
	return 0;
}
uint32_t gen_msg_service_data(uint8_t* data_ptr)
{
    service_data_t* ptr = (service_data_t*)data_ptr;
    if (data_ptr == NULL)
        return 0;
    ptr->type = REPORT_TYPE_SERVICE;
    ptr->device_id = BE_TO_LE32(g_deviceid);
    ptr->device_id_optional = BE_TO_LE32(0);
    ptr->device_type = DEVICE_TYPE_LOCK;
    ptr->lng = BE_TO_LE32(g_longitude);
    ptr->lat = BE_TO_LE32(g_latitude);
    ptr->service_data_type = 0x01;
    ptr->lock_status = LOCK_STATUS_LOCKED;
    ptr->lock_times = BE_TO_LE16(g_lock_times);
    ptr->batt_level = BE_TO_LE16(g_battery_volt);
    return sizeof(service_data_t);
}
uint32_t gen_msg_heartbeat_data(uint8_t* data_ptr)
{
    heartbeat_data_t* ptr = (heartbeat_data_t*)data_ptr;
    if (data_ptr == NULL)
        return 0;
    ptr->type = REPORT_TYPE_HEARTBEAT;
    ptr->device_id = BE_TO_LE32(g_deviceid);
    ptr->device_type = DEVICE_TYPE_LOCK;
    ptr->lng = BE_TO_LE32(g_longitude);
    ptr->lat = BE_TO_LE32(g_latitude);
    ptr->batt_level = BE_TO_LE16(g_battery_volt);
    ptr->version = 0x64; // 表示 1.00
    return sizeof(heartbeat_data_t);
}
uint32_t gen_msg_epile_data(uint8_t* data_ptr)
{
    uint32_t i;
    uint32_t bike_cnt;
    epile_data_t* ptr = (epile_data_t*)data_ptr;
    if (data_ptr == NULL)
        return 0;
    ptr->type = REPORT_TYPE_EPILE;
    ptr->device_id = BE_TO_LE32(g_deviceid);
    ptr->lng = BE_TO_LE32(g_longitude);
    ptr->lat = BE_TO_LE32(g_latitude);
    ptr->rssi = BE_TO_LE16(g_ble_rssi);
    ptr->batt_level = BE_TO_LE16(g_battery_volt);
    ptr->M_ident = 'M';
    // 获取周边车辆数目
    // 周边车辆的蓝牙MAC地址
    bike_cnt = gen_rand_range(1, BLE_MAC_ADDR_NUM_MAX);
    ALIOT_LOG_INFO("bike_cnt = %d", bike_cnt);
    for (i = 0; i < bike_cnt; i++)
    {
        simu_bike_mac_addr(&ptr->BLE_MAC[i]);
    }
    // 未使用的字节数,报文尾部的mac地址
    ALIOT_LOG_DEBUG("data len = %d", sizeof(epile_data_t) - 6 * (BLE_MAC_ADDR_NUM_MAX - bike_cnt));
    return (sizeof(epile_data_t) - 6 * (BLE_MAC_ADDR_NUM_MAX - bike_cnt));
}
uint32_t gen_msg_return_bike_requ_data(uint8_t* data_ptr)
{
    return_bike_requ_data_t* ptr = (return_bike_requ_data_t*)data_ptr;
    if (data_ptr == NULL)
        return 0;
    ptr->type = REPORT_TYPE_RETURN_BIKE;
    ptr->device_id = BE_TO_LE32(g_deviceid);
    ptr->device_id_optional = BE_TO_LE32(0);
    ptr->device_type = DEVICE_TYPE_LOCK;
    ptr->lng = BE_TO_LE32(g_longitude);
    ptr->lat = BE_TO_LE32(g_latitude);
    ptr->lock_status = LOCK_STATUS_LOCKED;
    ptr->lock_times = BE_TO_LE16(g_lock_times);
    ptr->batt_level = BE_TO_LE16(g_battery_volt);
    return sizeof(service_data_t);
}
