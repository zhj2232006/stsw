
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "lwip/sys.h"
#include "lwip/def.h"
#include "lwip/sockets.h"
#include "lwip/netdb.h"
#include "aliot_platform_os.h"
#include "aliot_platform_network.h"
#ifdef SOCK_DEBUG
    #define PLATFORM_SOCK_LOG(format, ...) aliot_platform_printf("[SOCK]%d %s " format "\r\n", /*__FILE__,*/ __LINE__, __FUNCTION__, ##__VA_ARGS__)
#else
    #define PLATFORM_SOCK_LOG(format, ...)
#endif


static uint32_t linux_get_time_ms(void)
{
    uint32_t time_ms;
    time_ms = xTaskGetTickCount();
    return time_ms;
}

static uint32_t linux_time_left(uint32_t t_end, uint32_t t_now)
{
    uint32_t t_left;

    if (t_end > t_now) {
        t_left = t_end - t_now;
    } else {
        t_left = 0;
    }

    return t_left;
}
intptr_t aliot_platform_tcp_establish(const char* host, uint16_t port)
{
    struct addrinfo hints;
    struct addrinfo* addrInfoList = NULL;
    struct addrinfo* cur = NULL;
    int fd = -1;
    int rc = -1;
    char service[6];

    memset(&hints, 0, sizeof(hints));

    PLATFORM_SOCK_LOG("establish tcp connection with server(host=%s port=%u)", host, port);

    hints.ai_family = AF_INET; //only IPv4
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    sprintf(service, "%u", port);

    if ((rc = getaddrinfo(host, service, &hints, &addrInfoList)) != 0) {
        PLATFORM_SOCK_LOG("getaddrinfo error");
        return 0;
    }

    for (cur = addrInfoList; cur != NULL; cur = cur->ai_next) {
        if (cur->ai_family != AF_INET) {
            PLATFORM_SOCK_LOG("socket type error");
            rc = 0;
            continue;
        }

        fd = socket(cur->ai_family, cur->ai_socktype, cur->ai_protocol);
        if (fd < 0) {
            PLATFORM_SOCK_LOG("create socket error");
            rc = 0;
            continue;
        }

        if (connect(fd, cur->ai_addr, cur->ai_addrlen) == 0) {
			PLATFORM_SOCK_LOG("connect socket %d ok", fd);
            rc = fd;
            break;
        }

        close(fd);
        PLATFORM_SOCK_LOG("connect error");
        rc = -1;
    }

    if (rc < 0) {
        PLATFORM_SOCK_LOG("fail to establish tcp");
    } else {
        PLATFORM_SOCK_LOG("success to establish tcp, fd=%d", rc);
    }
    freeaddrinfo(addrInfoList);

    return (intptr_t)rc;
}

int aliot_platform_tcp_destroy(uintptr_t fd)
{
    int rc;

    //Shutdown both send and receive operations.
    rc = shutdown((int)fd, 2);
    if (0 != rc) {
        PLATFORM_SOCK_LOG("shutdown error");
        return -1;
    }

    rc = close((int)fd);
    if (0 != rc) {
        PLATFORM_SOCK_LOG("closesocket error");
        return -1;
    }

    return 0;
}

int32_t aliot_platform_tcp_write(uintptr_t fd, const char* buf, uint32_t len, uint32_t timeout_ms)
{
    int ret, err_code;
    uint32_t len_sent;
    uint32_t t_end, t_left;
    fd_set sets;

    t_end = linux_get_time_ms() + timeout_ms;
    len_sent = 0;
    err_code = 0;
    ret = 1; //send one time if timeout_ms is value 0

    do {
        t_left = linux_time_left(t_end, linux_get_time_ms());

        if (0 != t_left) {
            struct timeval timeout;

            FD_ZERO(&sets);
            FD_SET(fd, &sets);

            timeout.tv_sec = t_left / 1000;
            timeout.tv_usec = (t_left % 1000) * 1000;

            ret = select(fd + 1, NULL, &sets, NULL, &timeout);
            if (ret > 0) {
                if (0 == FD_ISSET(fd, &sets)) {
                    PLATFORM_SOCK_LOG("Should NOT arrive");
                    //If timeout in next loop, it will not sent any data
                    ret = 0;
                    continue;
                }
            } else if (0 == ret) {
                PLATFORM_SOCK_LOG("select-write timeout %lu", fd);
                break;
            } else {
                if (EINTR == errno) {
                    PLATFORM_SOCK_LOG("EINTR be caught");
                    continue;
                }

                err_code = -1;
                PLATFORM_SOCK_LOG("select-write fail");
                break;
            }
        }

        if (ret > 0) {
            ret = send(fd, buf + len_sent, len - len_sent, 0);
            if (ret > 0) {
                len_sent += ret;
            } else if (0 == ret) {
                PLATFORM_SOCK_LOG("No data be sent");
            } else {
                if (EINTR == errno) {
                    PLATFORM_SOCK_LOG("EINTR be caught");
                    continue;
                }

                err_code = -1;
                PLATFORM_SOCK_LOG("send fail");
                break;
            }
        }
    } while ((len_sent < len) && (linux_time_left(t_end, linux_get_time_ms()) > 0));

    return len_sent;
}

int32_t aliot_platform_tcp_read(uintptr_t fd, char* buf, uint32_t len, uint32_t timeout_ms)
{
    int ret, err_code;
    uint32_t len_recv;
    uint32_t t_end, t_left;
    fd_set sets;
    struct timeval timeout;

    t_end = linux_get_time_ms() + timeout_ms;
    len_recv = 0;
    err_code = 0;

    do {
        t_left = linux_time_left(t_end, linux_get_time_ms());
        if (0 == t_left) {
            break;
        }
        FD_ZERO(&sets);
        FD_SET(fd, &sets);

        timeout.tv_sec = t_left / 1000;
        timeout.tv_usec = (t_left % 1000) * 1000;

        ret = select(fd + 1, &sets, NULL, NULL, &timeout);
        if (ret > 0) {
            ret = recv(fd, buf + len_recv, len - len_recv, 0);
            if (ret > 0) {
                len_recv += ret;
            } else if (0 == ret) {
                PLATFORM_SOCK_LOG("connection is closed");
                err_code = -1;
                break;
            } else {
                if (EINTR == errno) {
                    PLATFORM_SOCK_LOG("EINTR be caught");
                    continue;
                }
                PLATFORM_SOCK_LOG("send fail");
                err_code = -2;
                break;
            }
        } else if (0 == ret) {
            break;
        } else {
            PLATFORM_SOCK_LOG("select-recv fail");
            err_code = -2;
            break;
        }
    } while ((len_recv < len));

    //priority to return data bytes if any data be received from TCP connection.
    //It will get error code on next calling
    return (0 != len_recv) ? len_recv : err_code;
}
