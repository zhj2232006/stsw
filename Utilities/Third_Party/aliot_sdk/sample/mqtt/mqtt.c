
#include "aliot_platform.h"
#include "aliot_log.h"
#include "aliot_mqtt_client.h"
#include "aliot_auth.h"
#include "aliot_device.h"

//The product and device information from IOT console
#define IOT_PRODUCT_KEY "HiLixqlvIOf"
#define IOT_DEVICE_NAME "imt407g"
#define IOT_DEVICE_SECRET "9HWR8i1wF9qw2aEhm2mgoengSB4JOnfM"

#if ALIOT_AUTH_SKIP
    const char* g_al_iot_host = "public.iot-as-mqtt.cn-shanghai.aliyuncs.com";
    const uint16_t g_al_iot_port = 1883;
    const char* g_al_iot_id = "ZoySMnDiAxywLzzrJfcq0010f90800";
    const char* g_al_iot_token = "3301406099264797a49a67bef3995dfa";
#endif

//This is the pre-defined topic
#define TOPIC_UPDATE "/" IOT_PRODUCT_KEY "/" IOT_DEVICE_NAME "/update" // only for pub
#define TOPIC_ERROR "/" IOT_PRODUCT_KEY "/" IOT_DEVICE_NAME "/update/error" // only for pub
#define TOPIC_GET "/" IOT_PRODUCT_KEY "/" IOT_DEVICE_NAME "/get" // only for sub

//NOTE!!! you must define the following topic in IOT console before running this sample.
#define TOPIC_DATA "/" IOT_PRODUCT_KEY "/" IOT_DEVICE_NAME "/data" // for sub and pub

#define MSG_LEN_MAX         (256)


void event_handle(void* pcontext, void* pclient, aliot_mqtt_event_msg_pt msg)
{
    uint32_t packet_id = (uint32_t)msg->msg;
    aliot_mqtt_topic_info_pt topic_info = (aliot_mqtt_topic_info_pt)msg->msg;

    switch (msg->event_type)
    {
    case ALIOT_MQTT_EVENT_UNDEF:
        ALIOT_LOG_INFO("undefined event occur.");
        break;

    case ALIOT_MQTT_EVENT_DISCONNECT:
        ALIOT_LOG_INFO("MQTT disconnect.");
        break;

    case ALIOT_MQTT_EVENT_RECONNECT:
        ALIOT_LOG_INFO("MQTT reconnect.");
        break;

    case ALIOT_MQTT_EVENT_SUBCRIBE_SUCCESS:
        ALIOT_LOG_INFO("subscribe success, packet-id=%u", packet_id);
        break;

    case ALIOT_MQTT_EVENT_SUBCRIBE_TIMEOUT:
        ALIOT_LOG_INFO("subscribe wait ack timeout, packet-id=%u", packet_id);
        break;

    case ALIOT_MQTT_EVENT_SUBCRIBE_NACK:
        ALIOT_LOG_INFO("subscribe nack, packet-id=%u", packet_id);
        break;

    case ALIOT_MQTT_EVENT_UNSUBCRIBE_SUCCESS:
        ALIOT_LOG_INFO("unsubscribe success, packet-id=%u", packet_id);
        break;

    case ALIOT_MQTT_EVENT_UNSUBCRIBE_TIMEOUT:
        ALIOT_LOG_INFO("unsubscribe timeout, packet-id=%u", packet_id);
        break;

    case ALIOT_MQTT_EVENT_UNSUBCRIBE_NACK:
        ALIOT_LOG_INFO("unsubscribe nack, packet-id=%u", packet_id);
        break;

    case ALIOT_MQTT_EVENT_PUBLISH_SUCCESS:
        ALIOT_LOG_INFO("publish success, packet-id=%u", packet_id);
        break;

    case ALIOT_MQTT_EVENT_PUBLISH_TIMEOUT:
        ALIOT_LOG_INFO("publish timeout, packet-id=%u", packet_id);
        break;

    case ALIOT_MQTT_EVENT_PUBLISH_NACK:
        ALIOT_LOG_INFO("publish nack, packet-id=%u", packet_id);
        break;

    case ALIOT_MQTT_EVENT_PUBLISH_RECVEIVED:
        ALIOT_LOG_WARN("topic message arrived but without any related handle: topic=%.*s, topic_msg=%.*s",
                       topic_info->topic_len,
                       topic_info->ptopic,
                       topic_info->payload_len,
                       topic_info->payload);
        break;

    default:
        ALIOT_LOG_INFO("Should NOT arrive here.");
        break;
    }
}


void aliot_mqtt_msg_arrived(void* pcontext, void* pclient, aliot_mqtt_event_msg_pt msg)
{
    aliot_mqtt_topic_info_pt ptopic_info = (aliot_mqtt_topic_info_pt) msg->msg;

    //print topic name and topic message
    ALIOT_LOG_INFO("topic=%.*s, topic_msg=%.*s",
                   ptopic_info->topic_len,
                   ptopic_info->ptopic,
                   ptopic_info->payload_len,
                   ptopic_info->payload);
    if (strstr(ptopic_info->payload, "beepon"))
    {
//        beep_on();
        aliot_platform_msleep(200);
//        beep_off();
    }
}

int mqtt_client(void)
{
    int rc = 0, msg_len, cnt = 0;
    void* pclient;
    aliot_user_info_pt puser_info;
    aliot_mqtt_param_t mqtt_params;
    aliot_mqtt_topic_info_t topic_msg;
    char msg_pub[128];
    char* msg_buf = NULL, *msg_readbuf = NULL;

    if (NULL == (msg_buf = (char*)aliot_platform_malloc(MSG_LEN_MAX)))
    {
        ALIOT_LOG_ERROR("no enough memory %d", MSG_LEN_MAX);
        rc = -1;
        goto do_exit;
    }

    if (NULL == (msg_readbuf = (char*)aliot_platform_malloc(MSG_LEN_MAX)))
    {
        ALIOT_LOG_ERROR("no enough memory %d", MSG_LEN_MAX);
        rc = -1;
        goto do_exit;
    }

    /* Initialize device info */
    aliot_device_init();

    if (0 != aliot_set_device_info(IOT_PRODUCT_KEY, IOT_DEVICE_NAME, IOT_DEVICE_SECRET))
    {
        ALIOT_LOG_ERROR("set device info failed!");
        rc = -1;
        goto do_exit;
    }

    /* Device AUTH */
    if (0 != aliot_auth(aliot_get_device_info(), aliot_get_user_info()))
    {
        ALIOT_LOG_ERROR("AUTH request failed!");
        rc = -1;
        goto do_exit;
    }
	ALIOT_LOG_INFO("AUTH request success!");
    puser_info = aliot_get_user_info();

    /* Initialize MQTT parameter */
    memset(&mqtt_params, 0x0, sizeof(mqtt_params));

    mqtt_params.port = puser_info->port;
    mqtt_params.host = puser_info->host_name;
    mqtt_params.client_id = puser_info->client_id;
    mqtt_params.user_name = puser_info->user_name;
    mqtt_params.password = puser_info->password;
    mqtt_params.pub_key = puser_info->pubKey;

    mqtt_params.request_timeout_ms = 2000;
    mqtt_params.clean_session = 0;
    mqtt_params.keepalive_interval_ms = 6000;
    mqtt_params.pread_buf = msg_readbuf;
    mqtt_params.read_buf_size = MSG_LEN_MAX;
    mqtt_params.pwrite_buf = msg_buf;
    mqtt_params.write_buf_size = MSG_LEN_MAX;

    mqtt_params.handle_event.h_fp = event_handle;
    mqtt_params.handle_event.pcontext = NULL;


    /* Construct a MQTT client with specify parameter */
    pclient = aliot_mqtt_construct(&mqtt_params);
    if (NULL == pclient)
    {
        ALIOT_LOG_ERROR("MQTT construct failed");
        rc = -1;
        goto do_exit;
    }

    /* Subscribe the specific topic */
    rc = aliot_mqtt_subscribe(pclient, TOPIC_DATA, ALIOT_MQTT_QOS1, aliot_mqtt_msg_arrived, NULL);
    if (rc < 0)
    {
        aliot_mqtt_deconstruct(pclient);
        ALIOT_LOG_ERROR("ali_iot_mqtt_subscribe failed, rc = %d", rc);
        rc = -1;
        goto do_exit;
    }

    rc = aliot_mqtt_subscribe(pclient, TOPIC_GET, ALIOT_MQTT_QOS1, aliot_mqtt_msg_arrived, NULL);
    if (rc < 0)
    {
        aliot_mqtt_deconstruct(pclient);
        ALIOT_LOG_ERROR("ali_iot_mqtt_subscribe failed, rc = %d", rc);
        rc = -1;
        goto do_exit;
    }
    aliot_mqtt_yield(pclient, 1000);

    /* Initialize topic information */
    memset(&topic_msg, 0x0, sizeof(aliot_mqtt_topic_info_t));
    strcpy(msg_pub, "message: hello! start!");

    topic_msg.qos = ALIOT_MQTT_QOS1;
    topic_msg.retain = 0;
    topic_msg.dup = 0;
    topic_msg.payload = (void*)msg_pub;
    topic_msg.payload_len = strlen(msg_pub);

    do
    {
        /* Generate topic message */
        cnt++;
        msg_len = snprintf(msg_pub, sizeof(msg_pub), "{\"attr_name\":\"temperature\", \"attr_value\":\"%d\"}, \"attr_value2\":\"%d\"}", cnt, cnt);
        if (msg_len < 0)
        {
            ALIOT_LOG_ERROR("Error occur! Exit program");
            rc = -1;
            break;
        }

        topic_msg.payload = (void*)msg_pub;
        topic_msg.payload_len = msg_len;

        rc = aliot_mqtt_publish(pclient, TOPIC_DATA, &topic_msg);
        if (rc < 0)
        {
            ALIOT_LOG_ERROR("error occur when publish");
            rc = -1;
            break;
        }
        ALIOT_LOG_INFO("packet-id=%u, publish topic msg=%s", (uint32_t)rc, msg_pub);

        /* handle the MQTT packet received from TCP or SSL connection */
        aliot_mqtt_yield(pclient, 1000);

        //aliot_platform_msleep(1000);

    } while (cnt < 10);

    aliot_mqtt_unsubscribe(pclient, TOPIC_DATA);

    aliot_platform_msleep(200);

    aliot_mqtt_deconstruct(pclient);


do_exit:
    if (NULL != msg_buf)
    {
        aliot_platform_free(msg_buf);
    }

    if (NULL != msg_readbuf)
    {
        aliot_platform_free(msg_readbuf);
    }
    ALIOT_LOG_INFO("retcode = %d", rc);

    return rc;
}


#ifdef _WIN32
int main()
{
    while (true)
    {
    mqtt_client();
    }
    ALIOT_LOG_WARN("out of sample!");

    return 0;
}
#endif
