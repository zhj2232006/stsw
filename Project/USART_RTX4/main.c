/******************************************************************************

                  版权所有 (C) 2016

 ******************************************************************************
  文件   : tasks.c
  版本   : 初稿
  作者   : zhoujie
  日期   : 2016年6月1日
  修改   :
  描述   : tasks
  修改   :
  日期   : 2016年6月8日
    作者   : zhoujie
    内容   : 创建文件

******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include "system_stm32f4xx.h"
#include "math.h"
#include "RTL.h"
#include "RTX_UART.h"
#include "stm324xg_eval.h"

OS_TID t_sysmonit1;
OS_TID t_sysmonit2;
OS_TID t_sysmonit3;
OS_TID t_sysmonit4;

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
int fputc(int ch, FILE* f)
{
#if 1
    UART_msg_t msg;
    msg.data = ch;
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART */
    UART_send(__UART_CTRL1, &msg, 0xffff);
#endif
    return ch;
}

static OS_MUT os_print_mutex;

void init_os_debug_obj()
{
#if 1
    os_mut_init(&os_print_mutex);
#endif
}

#if 1
__task void task_sysmonit1(void)
{
#if 1
    printf("%s %d\r\n", __FUNCTION__, __LINE__);
#endif
    for (;;)
    {
        STM_EVAL_LEDOn(LED1);
        // 状态指示LED闪烁
        os_dly_wait(100);
        printf("%s\r\n", __FUNCTION__);
        STM_EVAL_LEDOff(LED1);
        os_dly_wait(200);
    }
}
__task void task_sysmonit2(void)
{
#if 1
    printf("%s %d\r\n", __FUNCTION__, __LINE__);
#endif
    for (;;)
    {
        STM_EVAL_LEDOn(LED2);
        // 状态指示LED闪烁
        os_dly_wait(300);
        printf("%s\r\n", __FUNCTION__);
        STM_EVAL_LEDOff(LED2);
        os_dly_wait(400);
    }
}
__task void task_sysmonit3(void)
{
#if 1
    printf("%s %d\r\n", __FUNCTION__, __LINE__);
#endif
    for (;;)
    {
        STM_EVAL_LEDOn(LED3);
        // 状态指示LED闪烁
        os_dly_wait(500);
        printf("%s\r\n", __FUNCTION__);
        STM_EVAL_LEDOff(LED3);
        os_dly_wait(600);
    }
}
__task void task_sysmonit4(void)
{
#if 1
    UART_msg_t msg;

#endif

#if 1
    printf("%s %d\r\n", __FUNCTION__, __LINE__);
#endif
    for (;;)
    {
        UART_receive(__UART_CTRL1, &msg, 0xffff);
        UART_send(__UART_CTRL1, &msg, 0xffff);
    }
}

#endif

__task void tasks_init(void)
{
    /* LEDs configuration ------------------------------------------------------*/
    STM_EVAL_LEDInit(LED1);
    STM_EVAL_LEDInit(LED2);
    STM_EVAL_LEDInit(LED3);

    UART_init(__UART_CTRL1, 115200);
    init_os_debug_obj();
    printf("RTX compiled at %s %s\r\n", __DATE__, __TIME__);
    printf("starting...\r\n");

    t_sysmonit1 = os_tsk_create(task_sysmonit1, 4);
    t_sysmonit2 = os_tsk_create(task_sysmonit2, 3);
    t_sysmonit3 = os_tsk_create(task_sysmonit3, 2);
    t_sysmonit4 = os_tsk_create(task_sysmonit4, 1);

    os_tsk_delete_self();
}

/*----------------------------------------------------------------------------
  Main: Initialize and start RTX Kernel
 *---------------------------------------------------------------------------*/
int main(void)
{
    SystemCoreClockUpdate();

    os_sys_init(tasks_init); /* Initialize RTX and start tasks_init    */
    return -1;
}
