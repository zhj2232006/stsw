#include <string.h>
#include <stdio.h>
#include "stm32f4xx.h"

#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif


#include "mbedtls/aes.h"
#ifdef MBEDTLS_AES_ENCRYPT_ALT

void mbedtls_aes_encrypt(mbedtls_aes_context* ctx,
                         const unsigned char input[16],
                         unsigned char output[16])
{

}
#endif /* MBEDTLS_AES_ENCRYPT_ALT */
#ifdef MBEDTLS_AES_DECRYPT_ALT

void mbedtls_aes_decrypt(mbedtls_aes_context* ctx,
                         const unsigned char input[16],
                         unsigned char output[16])
{
}
#endif /* MBEDTLS_AES_DECRYPT_ALT */
#ifdef MBEDTLS_AES_SETKEY_ENC_ALT

int mbedtls_aes_setkey_enc( mbedtls_aes_context *ctx, const unsigned char *key,
                    unsigned int keybits )
{
}
#endif /* MBEDTLS_AES_SETKEY_ENC_ALT */
#ifdef MBEDTLS_AES_SETKEY_DEC_ALT


int mbedtls_aes_setkey_dec( mbedtls_aes_context *ctx, const unsigned char *key,
                    unsigned int keybits )
{
}
#endif /* MBEDTLS_AES_SETKEY_DEC_ALT */

/*-----------------------------------------------------------*/

/**
  * @brief  Returns a 32-bit random number.
  * @param  arg not used
  * @retval 32-bit random number
  */
int RandVal(void* arg)
{
    uint32_t ret;

    /* Wait until random number is ready */
    while (RNG_GetFlagStatus(RNG_FLAG_DRDY) == RESET)
        ;

    /* Get the random number */
    ret = RNG_GetRandomNumber();

    /* Return the random number */
    return (ret);
}
#ifdef MBEDTLS_ENTROPY_HARDWARE_ALT

int mbedtls_hardware_poll(void* Data, unsigned char* Output, size_t Len, size_t* oLen)
{
    uint32_t index;
    uint32_t randomValue;

    for (index = 0; index < Len / 4; index++)
    {
        randomValue = RandVal(NULL);
        *oLen += 4;
        memset(&(Output[index * 4]), (int)randomValue, 4);
    }

    return 0;
}
#endif /* MBEDTLS_ENTROPY_HARDWARE_ALT */


