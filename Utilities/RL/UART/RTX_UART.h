/*----------------------------------------------------------------------------
 *      RL-ARM - UART
 *----------------------------------------------------------------------------
 *      Name:    RTX_UART.h
 *      Purpose: Header for UART Generic Layer Driver
 *      Rev.:    V4.70
 *----------------------------------------------------------------------------
 *      This code is part of the RealView Run-Time Library.
 *      Copyright (c) 2004-2014 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------*/

#include "RTL.h"      /* RTX kernel functions & defines      */
#include "UART_Cfg.h" /* UART Configuration                   */

#ifndef __RTX_UART_H
#define __RTX_UART_H

#ifdef __cplusplus
extern "C" {
#endif

#define __UART_CTRL1 1
#define __UART_CTRL2 2
#define __UART_CTRL3 3
#define __UART_CTRL4 4
#define __UART_CTRL5 5

#if (USE_UART_CTRL5)
#define UART_CTRL_MAX_NUM 5
#elif (USE_UART_CTRL4)
#define UART_CTRL_MAX_NUM 4
#elif(USE_UART_CTRL3)
#define UART_CTRL_MAX_NUM 3
#elif(USE_UART_CTRL2)
#define UART_CTRL_MAX_NUM 2
#elif(USE_UART_CTRL1)
#define UART_CTRL_MAX_NUM 1
#else
#error "No UART Controller defined"
#endif

/* UART message object structure */
typedef struct
{
    U8 data; /* Data field */
} UART_msg_t;

/* Externaly declared memory pool for UART messages, both transmit and receive*/
extern U32 UART_msg_pool[((sizeof(UART_msg_t) + 3) / 4) * (UART_CTRL_MAX_NUM * (UART_TX_BUF_MAX + UART_RX_BUF_MAX)) + 3];

/* Externaly declared mailbox, for UART transmit messages                     */
extern U32 UART_MBX_tx_ctrl[UART_CTRL_MAX_NUM][4 + UART_TX_BUF_MAX];

/* Externaly declared mailbox, for UART receive messages                      */
extern U32 UART_MBX_rx_ctrl[UART_CTRL_MAX_NUM][4 + UART_RX_BUF_MAX];

/* Semaphores used for protecting writing to UART hardware                    */
extern OS_SEM UART_wr_sem[UART_CTRL_MAX_NUM];

/* Error values that functions can return */
typedef enum
{
    UART_OK = 0,                /* No error                              */
    UART_NOT_IMPLEMENTED_ERROR, /* Function has not been implemented     */
    UART_MEM_POOL_INIT_ERROR,   /* Memory pool initialization error      */
    UART_BAUDRATE_ERROR,        /* Baudrate was not set                  */
    UART_TX_BUSY_ERROR,         /* Transmitting hardware busy            */
    UART_OBJECTS_FULL_ERROR,    /* No more rx or tx objects available    */
    UART_ALLOC_MEM_ERROR,       /* Unable to allocate memory from pool   */
    UART_DEALLOC_MEM_ERROR,     /* Unable to deallocate memory           */
    UART_TIMEOUT_ERROR,         /* Timeout expired                       */
    UART_UNEXIST_CTRL_ERROR,    /* Controller does not exist             */
    UART_UNEXIST_CH_ERROR,      /* Channel does not exist                */
} UART_ERROR_T;

/* Functions defined UART hardware driver (module UART_chip.c)                 */
extern UART_ERROR_T UART_hw_shutdown(U32 ctrl);
extern UART_ERROR_T UART_hw_setup(U32 ctrl);
extern UART_ERROR_T UART_hw_init(U32 ctrl, U32 baudrate);
extern UART_ERROR_T UART_hw_start(U32 ctrl);
extern UART_ERROR_T UART_hw_tx_empty(U32 ctrl);
extern UART_ERROR_T UART_hw_wr(U32 ctrl, UART_msg_t* msg);

/* Functions defined in module RTX_UART.c                                     */
UART_ERROR_T UART_init(U32 ctrl, U32 baudrate);
UART_ERROR_T UART_send(U32 ctrl, UART_msg_t* msg, U16 timeout);
UART_ERROR_T UART_receive(U32 ctrl, UART_msg_t* msg, U16 timeout);

#ifdef __cplusplus
}
#endif

#endif /* __RTX_UART_H */

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
