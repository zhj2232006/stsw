/*----------------------------------------------------------------------------
 *      RL-ARM - UART
 *----------------------------------------------------------------------------
 *      Name:    UART_STM32F4xx.c
 *      Purpose: UART Driver, Hardware specific module for ST STM32F103
 *      Rev.:    V4.70
 *----------------------------------------------------------------------------
 *      This code is part of the RealView Run-Time Library.
 *      Copyright (c) 2004-2013 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------*/

#include "RTL.h"      /* RTX kernel functions & defines      */
#include "RTX_UART.h" /* UART Generic functions & defines     */
#if defined(USE_STM324xG_EVAL)
    #include "stm324xg_eval.h"
#endif

/************************* UART Hardware Configuration ************************/

/*----------------------------------------------------------------------------
 *      UART RTX Hardware Specific Driver Functions
 *----------------------------------------------------------------------------
 *  Functions implemented in this module:
 *    static      void UART_set_timing      (U32 ctrl, U32 tseg1, U32 tseg2, U32 sjw, U32 brp)
 *    static UART_ERROR_T UART_hw_set_baudrate (U32 ctrl, U32 baudrate)
 *           UART_ERROR_T UART_hw_setup        (U32 ctrl)
 *           UART_ERROR_T UART_hw_init         (U32 ctrl, U32 baudrate)
 *           UART_ERROR_T UART_hw_start        (U32 ctrl)
 *           UART_ERROR_T UART_hw_testmode     (U32 ctrl, U32 testmode)
 *           UART_ERROR_T UART_hw_tx_empty     (U32 ctrl)
 *           UART_ERROR_T UART_hw_wr           (U32 ctrl,         UART_msg_t *msg)
 *    static      void UART_hw_rd           (U32 ctrl, U32 ch, UART_msg_t *msg)
 *           UART_ERROR_T UART_hw_set          (U32 ctrl,         UART_msg_t *msg)
 *           UART_ERROR_T UART_hw_rx_object    (U32 ctrl, U32 ch, U32 id, U32 object_para)
 *           UART_ERROR_T UART_hw_tx_object    (U32 ctrl, U32 ch,         U32 object_para)
 *    Interrupt fuction
 *---------------------------------------------------------------------------*/

/* Static functions used only in this module*/
/* UART Controller Register Addresses        */
USART_TypeDef* const UART_CTRL[] = { EVAL_COM1, EVAL_COM2, EVAL_COM3, EVAL_COM4, EVAL_COM5 };
GPIO_TypeDef* const UART_TX_PORT[] = { EVAL_COM1_TX_GPIO_PORT, EVAL_COM2_TX_GPIO_PORT, EVAL_COM3_TX_GPIO_PORT, EVAL_COM4_TX_GPIO_PORT, EVAL_COM5_TX_GPIO_PORT };
GPIO_TypeDef* const UART_RX_PORT[] = { EVAL_COM1_RX_GPIO_PORT, EVAL_COM2_RX_GPIO_PORT, EVAL_COM3_RX_GPIO_PORT, EVAL_COM4_RX_GPIO_PORT, EVAL_COM5_RX_GPIO_PORT };
const uint32_t UART_USART_CLK[] = { EVAL_COM1_CLK, EVAL_COM2_CLK, EVAL_COM3_CLK, EVAL_COM4_CLK, EVAL_COM5_CLK };
const uint32_t UART_TX_PORT_CLK[] = { EVAL_COM1_TX_GPIO_CLK, EVAL_COM2_TX_GPIO_CLK, EVAL_COM3_TX_GPIO_CLK, EVAL_COM4_TX_GPIO_CLK, EVAL_COM5_TX_GPIO_CLK };
const uint32_t UART_RX_PORT_CLK[] = { EVAL_COM1_RX_GPIO_CLK, EVAL_COM2_RX_GPIO_CLK, EVAL_COM3_RX_GPIO_CLK, EVAL_COM4_RX_GPIO_CLK, EVAL_COM5_RX_GPIO_CLK };
const uint16_t UART_TX_PIN[] = { EVAL_COM1_TX_PIN, EVAL_COM2_TX_PIN, EVAL_COM3_TX_PIN, EVAL_COM4_TX_PIN, EVAL_COM5_TX_PIN };
const uint16_t UART_RX_PIN[] = { EVAL_COM1_RX_PIN, EVAL_COM2_RX_PIN, EVAL_COM3_RX_PIN, EVAL_COM4_RX_PIN, EVAL_COM5_RX_PIN };
const uint16_t UART_TX_PIN_SOURCE[] = { EVAL_COM1_TX_SOURCE };
const uint16_t UART_RX_PIN_SOURCE[] = { EVAL_COM1_RX_SOURCE };
const uint16_t UART_TX_AF[] = { EVAL_COM1_TX_AF };
const uint16_t UART_RX_AF[] = { EVAL_COM1_RX_AF };
const uint16_t UART_IRQ_Priority[] = { 3, 4, 5, 6, 7 };
const uint16_t UART_IRQ_Number[] = { EVAL_COM1_IRQn, EVAL_COM2_IRQn, EVAL_COM3_IRQn, EVAL_COM4_IRQn, EVAL_COM5_IRQn };
#if 0
UART_ERROR_T UART_hw_shutdown(U32 ctrl)
{
    USART_Cmd(UART_CTRL[ctrl - 1], DISABLE);
    USART_ITConfig(UART_CTRL[ctrl - 1], USART_IT_RXNE, DISABLE);
    USART_ITConfig(UART_CTRL[ctrl - 1], USART_IT_TC, DISABLE);
    PeriphNVICConfig(UART_IRQ_Number[ctrl - 1], UART_IRQ_Priority[ctrl - 1], DISABLE);
    USART_DeInit(UART_CTRL[ctrl - 1]);
    PeriphClockConfig(UART_USART_CLK[ctrl - 1], DISABLE);
    return UART_OK;
}
#endif

void PeriphNVICConfig(u8 IRQChannel, u8 IRQPriority, FunctionalState IRQCmd)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = IRQChannel;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = IRQPriority; // 抢占优先级
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;                  // 响应优先级
    NVIC_InitStructure.NVIC_IRQChannelCmd = IRQCmd;
    NVIC_Init(&NVIC_InitStructure);
}
#ifdef NEVER

void PeriphClockConfig(uint32_t RCC_APBxPeriph, FunctionalState PeriphState)
{
    if (IS_RCC_AHB2_PERIPH(RCC_APBxPeriph))
    {
        RCC_AHB2PeriphClockCmd(RCC_APBxPeriph, PeriphState);
    }
    else if (IS_RCC_AHB1_CLOCK_PERIPH(RCC_APBxPeriph))
    {
        RCC_AHB1PeriphClockCmd(RCC_APBxPeriph, PeriphState);
    }
    else if (IS_RCC_APB1_PERIPH(RCC_APBxPeriph))
    {
        RCC_APB1PeriphClockCmd(RCC_APBxPeriph, PeriphState);
    }

    else if (IS_RCC_APB2_PERIPH(RCC_APBxPeriph))
    {
        RCC_APB2PeriphClockCmd(RCC_APBxPeriph, PeriphState);
    }
}
#endif /* NEVER */

/*--------------------------- UART_hw_setup ----------------------------------
 *
 *  Setup UART transmit and receive PINs and interrupt vectors
 *
 *  Parameter:  ctrl:       Index of the hardware UART controller (1 .. x)
 *
 *  Return:     UART_ERROR_T:  Error code
 *---------------------------------------------------------------------------*/

UART_ERROR_T UART_hw_setup(U32 ctrl)
{
    /* 1. Enable UART controller Clock         */
    GPIO_InitTypeDef GPIO_InitStructure;
    /* Enable GPIO clock */
    RCC_AHB1PeriphClockCmd(UART_TX_PORT_CLK[ctrl - 1], ENABLE);
    RCC_AHB1PeriphClockCmd(UART_RX_PORT_CLK[ctrl - 1], ENABLE);
    RCC_APB2PeriphClockCmd(UART_USART_CLK[ctrl - 1], ENABLE);
#if 0
    //PeriphClockEnable(RCC_APB2Periph_AFIO,ENABLE); // todo only USART1 need!
    /* 2. Setup UART Tx and Rx pins    */
    /* Configure USART Tx as alternate function push-pull */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Pin = UART_TX_PIN[ctrl - 1];
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(UART_TX_PORT[ctrl - 1], &GPIO_InitStructure);
    /* Configure USART Rx as input floating */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
    GPIO_InitStructure.GPIO_Pin = UART_RX_PIN[ctrl - 1];
    GPIO_Init(UART_RX_PORT[ctrl - 1], &GPIO_InitStructure);
#endif
    /* Connect PXx to USARTx_Tx*/
    GPIO_PinAFConfig(UART_TX_PORT[ctrl - 1], UART_TX_PIN_SOURCE[ctrl - 1], UART_TX_AF[ctrl - 1]);

    /* Connect PXx to USARTx_Rx*/
    GPIO_PinAFConfig(UART_RX_PORT[ctrl - 1], UART_RX_PIN_SOURCE[ctrl - 1], UART_RX_AF[ctrl - 1]);

    /* Configure USART Tx as alternate function  */
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;

    GPIO_InitStructure.GPIO_Pin = UART_TX_PIN[ctrl - 1];
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(UART_TX_PORT[ctrl - 1], &GPIO_InitStructure);

    /* Configure USART Rx as alternate function  */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Pin = UART_RX_PIN[ctrl - 1];
    GPIO_Init(UART_RX_PORT[ctrl - 1], &GPIO_InitStructure);

    return UART_OK;
}

/*--------------------------- UART_hw_init -----------------------------------
 *
 *  Initialize the UART hardware
 *
 *  Parameter:  ctrl:       Index of the hardware UART controller (1 .. x)
 *              baudrate:   Baudrate
 *
 *  Return:     UART_ERROR_T:  Error code
 *---------------------------------------------------------------------------*/

UART_ERROR_T UART_hw_init(U32 ctrl, U32 baudrate)
{
    USART_TypeDef* UARTx = UART_CTRL[ctrl - 1];
    USART_InitTypeDef USART_InitStructure;
    /* 复位串口 */
    USART_DeInit(UARTx);
    /* USART configuration */
    /* Set baudrate */
    USART_InitStructure.USART_BaudRate = baudrate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(UARTx, &USART_InitStructure);
    /* 3. Setup IRQ vector for UART interrupt */
    PeriphNVICConfig(UART_IRQ_Number[ctrl - 1], UART_IRQ_Priority[ctrl - 1], ENABLE);
    /* 4. Enable UART interrupt                */
    USART_ITConfig(UARTx, USART_IT_RXNE, ENABLE); //Enable receive interrupt
    USART_ITConfig(UARTx, USART_IT_TC, ENABLE);   //Enable transmit complete interrupt
    return UART_OK;
}

/*--------------------------- UART_hw_start ----------------------------------
 *
 *  reset UART initialisation mode
 *
 *  Parameter:  ctrl:       Index of the hardware UART controller (1 .. x)
 *
 *  Return:     UART_ERROR_T:  Error code
 *---------------------------------------------------------------------------*/

UART_ERROR_T UART_hw_start(U32 ctrl)
{
    USART_TypeDef* UARTx = UART_CTRL[ctrl - 1];
    /* Enable USART */
    USART_Cmd(UARTx, ENABLE);
    return UART_OK;
}

/*--------------------------- UART_hw_tx_empty -------------------------------
 *
 *  Check if transmit mailbox 0 is available for usage (empty)
 *
 *  Parameter:  ctrl:       Index of the hardware UART controller (1 .. x)
 *
 *  Return:     UART_ERROR_T:  Error code
 *---------------------------------------------------------------------------*/

UART_ERROR_T UART_hw_tx_empty(U32 ctrl)
{
    USART_TypeDef* UARTx = UART_CTRL[ctrl - 1];

    /* If semaphore is free, no wait */
    if ((os_sem_wait(UART_wr_sem[ctrl - 1], 0) != OS_R_TMO))
    {
        /* Transmit is empty */
        if (USART_GetFlagStatus(UARTx, USART_FLAG_TXE) == SET)
        {
            return UART_OK;
        }
        else
        {
            os_sem_send(UART_wr_sem[ctrl - 1]); /* Return a token back to semaphore    */
        }
    }

    return UART_TX_BUSY_ERROR;
}

/*--------------------------- UART_hw_wr -------------------------------------
 *
 *  Write UART_msg_t to the hardware registers of the requested controller
 *
 *  Parameter:  ctrl:       Index of the hardware UART controller (1 .. x)
 *              msg:        Pointer to UART message to be written to hardware
 *
 *  Return:     UART_ERROR_T:  Error code
 *---------------------------------------------------------------------------*/

UART_ERROR_T UART_hw_wr(U32 ctrl, UART_msg_t* msg)
{
    USART_TypeDef* UARTx = UART_CTRL[ctrl - 1];
    USART_SendData(UARTx, (U8)msg->data);
    return UART_OK;
}

/*--------------------------- UART_hw_rd -------------------------------------
 *
 *  Read UART_msg_t from the hardware registers of the requested controller
 *
 *  Parameter:  ctrl:       Index of the hardware UART controller (1 .. x)
 *              ch:         Ignored
 *              msg:        Pointer where UART message will be read
 *
 *  Return:     none
 *---------------------------------------------------------------------------*/

static void UART_hw_rd(U32 ctrl, U32 ch, UART_msg_t* msg)
{
    USART_TypeDef* UARTx = UART_CTRL[ctrl - 1];
    // UART receive
    msg->data = (U8)USART_ReceiveData(UARTx);
}

#if (USE_UART_CTRL1)

/**
  * @brief  This function handles USART1 global interrupt request.
  * @param  None
  * @retval : None
  */
void USART1_IRQHandler() //Serial Port 1 interrupt service routine
{
    UART_msg_t* ptrmsg;

    /* Receive Data register not empty interrupt */
    if (USART_GetITStatus(USART1, USART_IT_RXNE) == SET)
    {
        /* message pending ? */
        /* If the mailbox isn't full read the message from the hardware and
        send it to the message queue         */
        if (isr_mbx_check(UART_MBX_rx_ctrl[__UART_CTRL1 - 1]) > 0)
        {
            ptrmsg = _alloc_box(UART_msg_pool);

            if (ptrmsg)
            {
                /* Save read received message to mempool*/
                UART_hw_rd(__UART_CTRL1, 0, ptrmsg);
                /* save mempool pointer to mailbox */
                isr_mbx_send(UART_MBX_rx_ctrl[__UART_CTRL1 - 1], ptrmsg);
            }
        }

        // clear pending flag
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);
    }

    /* Transmit Data Register empty interrupt */

    /* Transmission complete interrupt */
    if (USART_GetITStatus(USART1, USART_IT_TC) == SET)
    {
        /* If there is a message in the mailbox ready for send, read the
        message from the mailbox and send it */
        if (isr_mbx_receive(UART_MBX_tx_ctrl[__UART_CTRL1 - 1], (void**)&ptrmsg) != OS_R_OK)
        {
            UART_hw_wr(__UART_CTRL1, ptrmsg);
            _free_box(UART_msg_pool, ptrmsg);
        }
        else
        {
            isr_sem_send(UART_wr_sem[__UART_CTRL1 - 1]); /* Return a token back to semaphore */
        }

        USART_ClearITPendingBit(USART1, USART_IT_TC);
    }
}

#endif

#if (USE_UART_CTRL2)

/**
  * @brief  This function handles USART2 global interrupt request.
  * @param  None
  * @retval : None
  */
void USART2_IRQHandler() //Serial Port 2 interrupt service routine
{
    UART_msg_t* ptrmsg;

    /* Receive Data register not empty interrupt */
    if (USART_GetITStatus(USART2, USART_IT_RXNE) == SET)
    {
        /* message pending ? */
        /* If the mailbox isn't full read the message from the hardware and
           send it to the message queue                                          */
        if (isr_mbx_check(UART_MBX_rx_ctrl[__UART_CTRL2 - 1]) > 0)
        {
            ptrmsg = _alloc_box(UART_msg_pool);

            if (ptrmsg)
            {
                UART_hw_rd(__UART_CTRL2, 0, ptrmsg); /* Read received message */
                isr_mbx_send(UART_MBX_rx_ctrl[__UART_CTRL2 - 1], ptrmsg);
            }
        }

        // clear pending flag
        USART_ClearITPendingBit(USART2, USART_IT_RXNE);
    }

    /* Transmit Data Register empty interrupt */

    /* Transmission complete interrupt */
    if (USART_GetITStatus(USART2, USART_IT_TC) == SET)
    {
        /* If there is a message in the mailbox ready for send, read the
           message from the mailbox and send it                                  */
        if (isr_mbx_receive(UART_MBX_tx_ctrl[__UART_CTRL2 - 1], (void**)&ptrmsg) != OS_R_OK)
        {
            UART_hw_wr(__UART_CTRL2, ptrmsg);
            _free_box(UART_msg_pool, ptrmsg);
        }
        else
        {
            isr_sem_send(UART_wr_sem[__UART_CTRL2 - 1]); /* Return a token back to semaphore */
        }

        USART_ClearITPendingBit(USART2, USART_IT_TC);
    }
}
#endif

#if (USE_UART_CTRL3)

/**
  * @brief  This function handles USART3 global interrupt request.
  * @param  None
  * @retval : None
  */
void USART3_IRQHandler() //Serial Port 3 interrupt service routine
{
    UART_msg_t* ptrmsg;

    /* Receive Data register not empty interrupt */
    if (USART_GetITStatus(USART3, USART_IT_RXNE) == SET)
    {
        /* message pending ? */
        /* If the mailbox isn't full read the message from the hardware and
               send it to the message queue                                          */
        if (isr_mbx_check(UART_MBX_rx_ctrl[__UART_CTRL3 - 1]) > 0)
        {
            ptrmsg = _alloc_box(UART_msg_pool);

            if (ptrmsg)
            {
                UART_hw_rd(__UART_CTRL3, 0, ptrmsg); /* Read received message */
                isr_mbx_send(UART_MBX_rx_ctrl[__UART_CTRL3 - 1], ptrmsg);
            }
        }

        // clear pending flag
        USART_ClearITPendingBit(USART3, USART_IT_RXNE);
    }

    /* Transmit Data Register empty interrupt */
    /* Transmission complete interrupt */
    if (USART_GetITStatus(USART3, USART_IT_TC) == SET)
    {
        /* If there is a message in the mailbox ready for send, read the
               message from the mailbox and send it                                  */
        if (isr_mbx_receive(UART_MBX_tx_ctrl[__UART_CTRL3 - 1], (void**)&ptrmsg) != OS_R_OK)
        {
            UART_hw_wr(__UART_CTRL3, ptrmsg);
            _free_box(UART_msg_pool, ptrmsg);
        }
        else
        {
            isr_sem_send(UART_wr_sem[__UART_CTRL3 - 1]); /* Return a token back to semaphore */
        }

        USART_ClearITPendingBit(USART3, USART_IT_TC);
    }
}
#endif

#if (USE_UART_CTRL4)
void UART4_IRQHandler() //Serial Port 4 interrupt service routine
{
    UART_msg_t* ptrmsg;

    /* Receive Data register not empty interrupt */
    if (USART_GetITStatus(UART4, USART_IT_RXNE) == SET)
    {
        /* message pending ? */
        /* If the mailbox isn't full read the message from the hardware and
               send it to the message queue                                          */
        if (isr_mbx_check(UART_MBX_rx_ctrl[__UART_CTRL4 - 1]) > 0)
        {
            ptrmsg = _alloc_box(UART_msg_pool);

            if (ptrmsg)
            {
                UART_hw_rd(__UART_CTRL4, 0, ptrmsg); /* Read received message */
                isr_mbx_send(UART_MBX_rx_ctrl[__UART_CTRL4 - 1], ptrmsg);
            }
        }

        // clear pending flag
        USART_ClearITPendingBit(UART4, USART_IT_RXNE);
    }

    /* Transmit Data Register empty interrupt */
    /* Transmission complete interrupt */
    if (USART_GetITStatus(UART4, USART_IT_TC) == SET)
    {
        /* If there is a message in the mailbox ready for send, read the
               message from the mailbox and send it                                  */
        if (isr_mbx_receive(UART_MBX_tx_ctrl[__UART_CTRL4 - 1], (void**)&ptrmsg) != OS_R_OK)
        {
            UART_hw_wr(__UART_CTRL4, ptrmsg);
            _free_box(UART_msg_pool, ptrmsg);
        }
        else
        {
            isr_sem_send(UART_wr_sem[__UART_CTRL4 - 1]); /* Return a token back to semaphore */
        }

        USART_ClearITPendingBit(UART4, USART_IT_TC);
    }
}

#endif

#if (USE_UART_CTRL5)
void UART5_IRQHandler() //Serial Port 5 interrupt service routine
{
    UART_msg_t* ptrmsg;

    /* Receive Data register not empty interrupt */
    if (USART_GetITStatus(UART5, USART_IT_RXNE) == SET)
    {
        /* message pending ? */
        /* If the mailbox isn't full read the message from the hardware and
               send it to the message queue                                          */
        if (isr_mbx_check(UART_MBX_rx_ctrl[__UART_CTRL5 - 1]) > 0)
        {
            ptrmsg = _alloc_box(UART_msg_pool);

            if (ptrmsg)
            {
                UART_hw_rd(__UART_CTRL5, 0, ptrmsg); /* Read received message */
                isr_mbx_send(UART_MBX_rx_ctrl[__UART_CTRL5 - 1], ptrmsg);
            }
        }

        // clear pending flag
        USART_ClearITPendingBit(UART5, USART_IT_RXNE);
    }

    /* Transmit Data Register empty interrupt */
    /* Transmission complete interrupt */
    if (USART_GetITStatus(UART5, USART_IT_TC) == SET)
    {
        /* If there is a message in the mailbox ready for send, read the
               message from the mailbox and send it                                  */
        if (isr_mbx_receive(UART_MBX_tx_ctrl[__UART_CTRL5 - 1], (void**)&ptrmsg) != OS_R_OK)
        {
            UART_hw_wr(__UART_CTRL5, ptrmsg);
            _free_box(UART_msg_pool, ptrmsg);
        }
        else
        {
            isr_sem_send(UART_wr_sem[__UART_CTRL5 - 1]); /* Return a token back to semaphore */
        }

        USART_ClearITPendingBit(UART5, USART_IT_TC);
    }
}

#endif

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
