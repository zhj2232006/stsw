#include <process.h>
#include <windows.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <memory.h>
#include "aliot_platform_os.h"

#ifdef OS_DEBUG
#define PLATFORM_WINOS_PERROR(format, ...)  aliot_platform_printf("[OS]%u %s " format "\n", __LINE__, __FUNCTION__, ##__VA_ARGS__)
#define PLATFORM_OS_PDEBUG(format, ...) aliot_platform_printf("[OS]%u %s " format "\n", __LINE__, __FUNCTION__, ##__VA_ARGS__)

#else
#define PLATFORM_WINOS_PERROR(format, ...) 
#define PLATFORM_OS_PDEBUG(format, ...) 
#endif

void* aliot_platform_mutex_create(void)
{
    HANDLE mutex;
    if (NULL == (mutex = CreateMutex(NULL, FALSE, NULL)))
    {
        PLATFORM_WINOS_PERROR("create mutex error");
    }
    return mutex;
}

void aliot_platform_mutex_destroy(_IN_ void* mutex)
{
    if (0 == CloseHandle(mutex))
    {
        PLATFORM_WINOS_PERROR("destroy mutex error");
    }
}

void aliot_platform_mutex_lock(_IN_ void* mutex)
{
    if (WAIT_FAILED == WaitForSingleObject(mutex, INFINITE))
    {
        PLATFORM_WINOS_PERROR("lock mutex error");
    }
}

void aliot_platform_mutex_unlock(_IN_ void* mutex)
{
    if (0 == ReleaseMutex(mutex))
    {
        PLATFORM_WINOS_PERROR("unlock mutex error");
    }
}

void* aliot_platform_malloc(_IN_ uint32_t size)
{
    void* ptr = malloc(size);
    PLATFORM_OS_PDEBUG("addr=%#x, size=%d", ptr, size);
    return ptr;
}

void aliot_platform_free(_IN_ void* ptr)
{
    PLATFORM_OS_PDEBUG("addr=%#x", ptr);
    return free(ptr);
}

int aliot_platform_ota_start(const char* md5, uint32_t file_size)
{
    printf("this interface is NOT support yet.");
    return -1;
}

int aliot_platform_ota_write(_IN_ char* buffer, _IN_ uint32_t length)
{
    printf("this interface is NOT support yet.");
    return -1;
}

int aliot_platform_ota_finalize(_IN_ int stat)
{
    printf("this interface is NOT support yet.");
    return -1;
}

uint32_t aliot_platform_time_get_ms(void)
{
    return (uint32_t)(GetTickCount());
}

void aliot_platform_msleep(_IN_ uint32_t ms)
{
    Sleep(ms);
}

static void platform_printf(_IN_ const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    fflush(stdout);
}
void aliot_platform_printf(char* fmt, ...)
{
    static char buf[1024];
    struct tm* t_tm;
    time_t t;
    va_list arg_lists;
    va_start(arg_lists, fmt);
    (void)vsnprintf((char*)buf, (size_t)sizeof(buf), (char const*)fmt, arg_lists);
    va_end(arg_lists);
#if 1
    // get timestamp
    t = time(NULL);
    // convert time to calendar
    t_tm = localtime(&t);
    t_tm->tm_year += 1900;
    t_tm->tm_mon += 1; /* tm_mon: 0~11 */
    printf("[%04d-%02d-%02d %02d:%02d:%02d]%s", t_tm->tm_year, t_tm->tm_mon, t_tm->tm_mday,
           t_tm->tm_hour, t_tm->tm_min, t_tm->tm_sec, buf);
#else
	printf("%s", buf);
#endif
}

/*
hex形式打印 u8 pdata[i] 的前n个元素
*/
void aliot_platform_printhex(const int8_t* title, uint8_t* pdata, uint32_t count)
{
    uint32_t i;
    if (count == 0)
        return;
    if (title != NULL)
        platform_printf("%s: ", title);
    for (i = 0; i < count; i++)
        platform_printf("%02X ", *(pdata + i));
    platform_printf("(%dB)\r\n", count);
}

char* aliot_platform_module_get_pid(char pid_str[])
{
    return NULL;
}
