/*----------------------------------------------------------------------------
 *      RL-ARM - UART
 *----------------------------------------------------------------------------
 *      Name:    UART_Cfg.h
 *      Purpose: Header for UART Configuration Settings
 *      Rev.:    V4.70
 *----------------------------------------------------------------------------
 *      This code is part of the RealView Run-Time Library.
 *      Copyright (c) 2004-2013 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------*/

#ifndef _UART_CFG_H
#define _UART_CFG_H

// *** <<< Use Configuration Wizard in Context Menu >>> ***

// <e0> Use UART Controller 1
// </e>
// <e1> Use UART Controller 2
// </e>
// <e2> Use UART Controller 3
// </e>
// <e3> Use UART Controller 4
// </e>
#define USE_UART_CTRL1 1
#define USE_UART_CTRL2 0
#define USE_UART_CTRL3 0
#define USE_UART_CTRL4 0
#define USE_UART_CTRL5 0

// <o0> Number of transmit objects for controllers <1-1024>
//      <i> Determines the size of software message buffer for transmitting.
//      <i> Default: 20
// <o1> Number of receive objects for controllers <1-1024>
//      <i> Determines the size of software message buffer for receiving.
//      <i> Default: 20
#define UART_TX_BUF_MAX 100
#define UART_RX_BUF_MAX 100

// *** <<< End of Configuration section             >>> ***

#endif /* _UART_CFG_H */

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
