/******************************************************************************

                  版权所有 (C) 2017, 金通科技

 ******************************************************************************
  文件   : smartbike_iot.h
  版本   : 初稿
  作者   : zhoujie
  日期   : 2017年7月17日
  修改   :
  描述   : smartbike_iot.c 的头文件
  函数列表   :
  修改   :
  日期   : 2017年7月17日
    作者   : zhoujie
    内容   : 创建文件

******************************************************************************/
#ifndef __SMARTBIKE_IOT_H__
#define __SMARTBIKE_IOT_H__
#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */
#define REPORT_TYPE_SERVICE 0
#define REPORT_TYPE_HEARTBEAT 1
#define REPORT_TYPE_EPILE 2
#define REPORT_TYPE_RETURN_BIKE 3
#define DEVICE_TYPE_LOCK 1
#define DEVICE_TYPE_EPILE 2
#define LOCK_STATUS_UNLOCKED 1
#define LOCK_STATUS_LOCKED 2
#define BLE_MAC_ADDR_NUM_MAX 39 // 最多检测提取周边车辆数目最大值
#pragma pack(push)
#pragma pack(1)
// 业务数据上报
typedef struct
{
    uint8_t type;                // 业务类型 0：业务数据上报
    uint32_t device_id;          // 设备ID
    uint8_t lock_status;         // 锁状态
    uint8_t device_type;         // 设备类型 1:蓝?锁 2:电?桩
    uint8_t service_data_type;   // 业务上报数据类型 0:强拆报警 1:租车数据 2:还车数据 3:租车异常数据 4:还车异常 5:临时停车6:继续骑?
    uint32_t lng;                // 经度坐标（乘10^6）
    uint32_t lat;                // 纬度坐标（乘10^6）
    uint32_t device_id_optional; // 电?桩Id[选填] 例如：23417890876
    uint16_t lock_times;         // ?命周期[开关锁次数]例如：1000次
    uint16_t batt_level;         // 电池电量 例如：3636mv
} service_data_t;
// 二、心跳包( 电子桩每1小时、锁每2小时上报)
typedef struct
{
    uint8_t type;        // 业务类型(值固定为1) 1：?跳包数据
    uint32_t device_id;  // 设备ID 例如：48798932
    uint8_t device_type; // 设备类型 1:蓝?锁 2:电?桩
    uint32_t lng;        // 经度坐标（乘10^6） 例如：116.443363
    uint32_t lat;        // 纬度坐标（乘10^6） 例如：39.972244
    uint16_t batt_level; // 电池电量 例如：3636mv
    uint8_t version; // 版本号 d100 表示 v1.00 
} heartbeat_data_t;
typedef struct
{
    uint8_t MAC[6];
} BLE_MAC_Addr_t;
// 三、电子桩网点巡检（后台查询通知触发上报）
typedef struct
{
    uint8_t type;                                 // 业务类型(值固定为2)
    uint32_t device_id;                           // 设备ID
    uint32_t lng;                                 // 经度坐标（乘10^6
    uint32_t lat;                                 // 纬度坐标（乘10^6）
    uint8_t rssi;                                 // 电?桩信号强弱（rssi） 例如：-56
    uint16_t batt_level;                          // 电?桩型号强弱（rssi） 例如：-56
    uint8_t M_ident;                              // 车辆列表标识(值固定为M) M
    BLE_MAC_Addr_t BLE_MAC[BLE_MAC_ADDR_NUM_MAX]; // 车辆MAC地址列表每个MAC地址6长度字节
} epile_data_t;
// 四、还车请求
typedef struct
{
    uint8_t type;                // 业务类型 3 还车请求
    uint32_t device_id;          // 设备ID
    uint8_t lock_status;         // 锁状态
    uint8_t device_type;         // 设备类型 1:蓝?锁 2:电?桩
    uint32_t lng;                // 经度坐标（乘10^6）
    uint32_t lat;                // 纬度坐标（乘10^6）
    uint32_t device_id_optional; // 电?桩Id[选填] 例如：23417890876
    uint16_t lock_times;         // ?命周期[开关锁次数]例如：1000次
    uint16_t batt_level;         // 电池电量 例如：3636mv
} return_bike_requ_data_t;
// 返回响应
typedef struct
{
    uint8_t type;           // 0 1 业务类型(值固定为3) 3
    uint8_t request_status; //1 1 请求状态 0:请求失败 1：请求成功
} return_bike_resp_data_t;
#pragma pack(pop)
extern uint32_t gen_msg_epile_data(uint8_t* data_ptr);
extern uint32_t gen_msg_heartbeat_data(uint8_t* data_ptr);
extern uint32_t gen_msg_service_data(uint8_t* data_ptr);
#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */
#endif /* __SMARTBIKE_IOT_H__ */
