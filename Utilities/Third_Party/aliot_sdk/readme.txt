version: v1.0.1_release

实现 platform 目录下 network,os 中的接口(ssl目录中的接口可暂时不实现),然后将src目录下所有相关文件加入编译,但mbedtls暂时不要加入，
专门起一个任务，调用 mqtt_client() 这个函数(smartbike_iot.c)。
改id：device_id.h
修改是否支持SSL: aliot_platform.h 

如果实现了 platform 下的接口之后，单片机就可以运行上暂时不支持ssl的版本了，先在windows上运行编译生成的 mqtt.exe，得到 
iot-id=...
iot-token=...
host=....
port=...
将这些信息填写到 device_id.c 这个文件，同时将 ALIOT_AUTH_SKIP 这个宏定义为1.

单片机平台下调试信息暂未实现。

